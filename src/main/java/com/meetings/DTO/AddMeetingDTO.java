package com.meetings.DTO;

import javax.validation.constraints.Future;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.Date;

public class AddMeetingDTO {

    @Future(message = "Data nie może być z przeszłości")
    private Date begin;

    @Min(value = 1, message = "Czas spotkania nie może być na minusie")
    @Max(120)
    private int duration;

    @Size(min = 3, max = 200, message = "Opis pokoju powinien sie z od 3 do 200 znaków")
    private String description;

    @Min(value = 0, message = "minimalny numer pokoju to 0")
    private int room;

    public AddMeetingDTO() {
    }

    public AddMeetingDTO(Date begin, int duration, String description, int room) {
        this.begin =  begin;
        this.duration = duration;
        this.description = description;
        this.room = room;
    }

    public Date getBegin() {
        return begin;
    }

    public int getDuration() {
        return duration;
    }

    public String getDescription() {
        return description;
    }

    public int getRoom() {
        return room;
    }
}