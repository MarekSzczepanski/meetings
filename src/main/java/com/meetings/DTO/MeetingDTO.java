package com.meetings.DTO;

import com.meetings.Model.Meeting;
import org.joda.time.DateTime;

import java.util.Date;

public class MeetingDTO {
    private Date begin;
    private Date end;
    private String description;
    private int room;

    public MeetingDTO() {
    }

    public MeetingDTO(Meeting meeting) {
        this.begin = meeting.getBegin();
        this.end = meeting.getEnd();
        this.description = meeting.getDescription();
        this.room = meeting.getRoom();
    }

    public MeetingDTO(AddMeetingDTO addMeetingDTO) {
        this.begin = addMeetingDTO.getBegin();
        this.end = new DateTime(addMeetingDTO.getBegin()).plusMinutes(addMeetingDTO.getDuration()).toDate();
        this.description = addMeetingDTO.getDescription();
        this.room = addMeetingDTO.getRoom();
    }

    public Date getBegin() {
        return begin;
    }

    public Date getEnd() {
        return end;
    }

    public String getDescription() {
        return description;
    }

    public int getRoom() {
        return room;
    }
}