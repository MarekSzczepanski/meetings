package com.meetings.Exception;

public class RoomBusyException extends Exception {
    public RoomBusyException(String message) {
        super("Pokój zajęty. Wolny termin: " + message);
    }
}