package com.meetings.Service;

import com.meetings.DTO.AddMeetingDTO;
import com.meetings.DTO.MeetingDTO;
import com.meetings.Exception.RoomBusyException;

import java.util.List;

public interface MeetingsService {
    List<MeetingDTO> getMeetings();
    Long addMeeting(AddMeetingDTO addMeetingDTO) throws RoomBusyException;
}