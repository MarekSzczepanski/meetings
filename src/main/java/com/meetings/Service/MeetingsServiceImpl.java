package com.meetings.Service;

import com.meetings.DTO.AddMeetingDTO;
import com.meetings.DTO.MeetingDTO;
import com.meetings.Dao.MeetingsDao;
import com.meetings.Exception.RoomBusyException;
import com.meetings.Model.Meeting;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MeetingsServiceImpl implements MeetingsService{
    private final MeetingsDao meetingsDao;

    @Autowired
    public MeetingsServiceImpl(MeetingsDao meetingsDao) {
        this.meetingsDao = meetingsDao;
    }

    public List<MeetingDTO> getMeetings(){
        List<Meeting> meetings = meetingsDao.findAll();
        List<MeetingDTO> meetingDTOS =new ArrayList<>(meetings.size());
        meetings.forEach(meeting -> meetingDTOS.add(new MeetingDTO(meeting)));
        return meetingDTOS;
    }

    public Long addMeeting(AddMeetingDTO addMeetingDTO) throws RoomBusyException {
        MeetingDTO meetingDTO = new MeetingDTO(addMeetingDTO);
        List<Meeting> meetings = meetingsDao.findByRoom(addMeetingDTO.getRoom());
        for(int i=0; i<meetings.size();i++){
            if(meetings.stream().anyMatch(meeting ->
                    (!meeting.getBegin().after(meetingDTO.getBegin()) & !meeting.getEnd().before( meetingDTO.getBegin())) ||
                            (!meeting.getBegin().after(meetingDTO.getEnd()) & !meeting.getEnd().before( meetingDTO.getEnd())) ||
                            (!meeting.getBegin().before(meetingDTO.getBegin()) & !meeting.getEnd().after( meetingDTO.getEnd()))
            )){
                for(int j=i;j<meetings.size()-1;j++){
                    if(meetings.get(j).getEnd().after(meetingDTO.getBegin())
                            && meetings.get(j+1).getBegin().after(meetingDTO.getEnd())){
                        DateTime freeTerm = new DateTime(meetings.get(j).getEnd()).plusMinutes(1);
                        throw new RoomBusyException(freeTerm.toString("HH:mm dd MMMM yyyy HH:mm"));
                    }
                }
                DateTime freeTerm = new DateTime(meetings.get(meetings.size()-1).getEnd()).plusMinutes(1);
                throw new RoomBusyException(freeTerm.toString("dd MMMM yyyy HH:mm"));
            }
        }
        Meeting meeting = new Meeting(meetingDTO.getBegin(), meetingDTO.getEnd(), meetingDTO.getDescription(), meetingDTO.getRoom());
        meeting = meetingsDao.addMeeting(meeting);
        return meeting.getId();
    }
}