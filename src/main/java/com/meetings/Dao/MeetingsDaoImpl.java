package com.meetings.Dao;

import com.meetings.Model.Meeting;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Repository
public class MeetingsDaoImpl implements MeetingsDao {
    private List<Meeting> meetings = new ArrayList<>();
    private static final AtomicLong count = new AtomicLong(0);

    @Override
    public List<Meeting> findAll() {
        return meetings;
    }

    @Override
    public Meeting addMeeting(Meeting meeting) {
        meeting.setId(count.incrementAndGet());
        meetings.add(meeting);
        return meeting;
    }

    @Override
    public List<Meeting> findByRoom(int room) {
        List<Meeting> filteredRoom = meetings.stream().filter(meeting -> meeting.getRoom() == room).collect(Collectors.toList());
        Collections.sort(filteredRoom);
        return filteredRoom;
    }
}