package com.meetings.Dao;

import com.meetings.Model.Meeting;

import java.util.List;

public interface MeetingsDao {
    List<Meeting> findAll();
    Meeting addMeeting(Meeting meeting);
    List<Meeting> findByRoom(int room);
}