package com.meetings.Controller;

import com.meetings.DTO.AddMeetingDTO;
import com.meetings.Exception.RoomBusyException;
import com.meetings.Service.MeetingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class MeetingsController {
    private final MeetingsService meetingsService;

    @Autowired
    public MeetingsController(MeetingsService meetingsService) {
        this.meetingsService = meetingsService;
    }

    @RequestMapping(method = RequestMethod.GET)
    ResponseEntity<?> getMeetings(){
        return ResponseEntity.ok(meetingsService.getMeetings());
    }

    @RequestMapping(method = RequestMethod.POST)
    ResponseEntity<?> addMeeting(@RequestBody @Valid AddMeetingDTO meeting) throws RoomBusyException {
        try {
            return new ResponseEntity<>(meetingsService.addMeeting(meeting), HttpStatus.OK);
        } catch (RoomBusyException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}