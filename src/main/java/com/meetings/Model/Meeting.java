package com.meetings.Model;

import java.util.Date;

public class Meeting implements Comparable<Meeting> {
    private Long id;
    private Date begin;
    private Date end;
    private String description;
    private int room;

    public Meeting() {
    }

    public Meeting(Date begin, Date end, String description, int room) {
        this.begin = begin;
        this.end = end;
        this.description = description;
        this.room = room;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getBegin() {
        return begin;
    }

    public void setBegin(Date begin) {
        this.begin = begin;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getRoom() {
        return room;
    }

    public void setRoom(int room) {
        this.room = room;
    }

    @Override
    public int compareTo(Meeting o) {
        return getBegin().compareTo(o.begin);
    }
}